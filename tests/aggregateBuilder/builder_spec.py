import unittest
from unittest.mock import Mock
from entities import AggregateRoot, DomainEvent
from fsm import EventSourcingMachine
from builder import AggregateRootBuilder
import datetime
import pymongo
import bson

class StartEvent(DomainEvent):

    def __init__(self, id, event_data):
        DomainEvent.__init__(self, "start", id, event_data)

    def apply(self, entity):
        entity['startTime'] = self._data['startTime']

class StopEvent(DomainEvent):

    def __init__(self, id, event_data):
        DomainEvent.__init__(self, "stop", id, event_data)

    def apply(self, entity):
        entity['stopTime'] = self._data['stopTime']

class SimpleEVSMachine(EventSourcingMachine):
    def __init__(self):
        EventSourcingMachine.__init__(self)
        self.add_state("Inicio", initial_state=True)
        self.add_state("Esperando")
        self.add_state("Fin")

        self.add_transition("start", "Inicio", "Esperando")
        self.add_transition("stop", "Esperando", "Fin")


class BuilderAggregateRootTestCase(unittest.TestCase):
    """
    Test unitario Aggregate Root
    """
    def setUp(self):
        self._db = pymongo.MongoClient().get_database('evs_clitellum')
        self._event = StartEvent(bson.ObjectId(), {"startTime": datetime.datetime.utcnow()})
        self._event_db = {"_id" : "5a1c3eec65a7d217810c4a61", \
                        "_t" : { "name" : "StartEvent", \
                                 "module" : "aggregateBuilder.builder_spec"}, \
                        "entity_id" : "5a1c3eec65a7d217810c4a60", \
                        "version" : 1511800556, \
                        "event" : { "startTime" : "2017-11-27T16:35:56.032+0000"}}

        self._repository = Mock()
        self._repository.get_entity_by_id = Mock(return_value=None)
        self._repository.get_events = Mock(return_value=[self._event_db])
        self._builder = AggregateRootBuilder(self._repository, SimpleEVSMachine)

    def test_build(self):
        
        root = self._builder.set_entity(bson.ObjectId()) \
                            .add_event(self._event) \
                            .build()

        self.assertEqual(root.get_state(), "Esperando")

    def test_build_stop_on_rejection(self):
        event = StartEvent(bson.ObjectId(), {"startTime": datetime.datetime.utcnow()})
        root = self._builder.set_entity(bson.ObjectId()) \
                            .set_stop_on_rejection() \
                            .add_event(event) \
                            .build()

        event2 = StopEvent(bson.ObjectId(), {"stopTime": datetime.datetime.utcnow()})
        root.add_event(event2)

        self._repository.save_event.assert_called_with(event2, root.get_id())
        self.assertEqual(root.get_state(), "Fin")
