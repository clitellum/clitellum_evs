"""
Test Aggregate Root
"""

import unittest
from unittest.mock import Mock
from entities import AggregateRoot, DomainEvent
from rejection.engines import RejectionEventException, StopOnErrorRejectionEngine

from fsm import EventSourcingMachine
import datetime

class StartEvent(DomainEvent):

    def __init__(self, id, event_data):
        DomainEvent.__init__(self, "start", id, event_data)

    def apply(self, entity):
        entity['startTime'] = self._data['startTime']


class AggregateRootTestCase(unittest.TestCase):
    """
    Test unitario Aggregate Root
    """
    def setUp(self):
        self._entity = {}

        self._machine = EventSourcingMachine()

        self._machine.add_state("Inicio", initial_state=True)
        self._machine.add_state("Esperando")
        self._machine.add_state("Fin")

        self._machine.add_transition("start", "Inicio", "Esperando")
        self._machine.add_transition("stop", "Esperando", "Fin")
        self._machine.set_state("Inicio")
        self._repository = Mock()
        self._snapshot = Mock()
        self._aggregateRoot = AggregateRoot('id', self._entity, self._machine, "Inicio", self._repository, self._snapshot)

    def test_apply_event(self):
        start_event = StartEvent('id', {'startTime': datetime.datetime.utcnow()})
        self._aggregateRoot.apply_event(start_event)

        self.assertEqual(self._aggregateRoot.get_state(), "Esperando")

    def test_apply_error_event(self):
        start_event = StartEvent('id', {'startTime': datetime.datetime.utcnow()})
        self._aggregateRoot.apply_event(start_event)

        start_event2 = StartEvent('id', {'startTime': datetime.datetime.utcnow()})
        self._aggregateRoot.apply_event(start_event2)

        self.assertEqual(self._aggregateRoot.get_state(), "Esperando")

    def test_apply_stop_on_error_event(self):
        self._aggregateRoot.set_rejection_engine(StopOnErrorRejectionEngine())

        start_event = StartEvent('id', {'startTime': datetime.datetime.utcnow()})
        self._aggregateRoot.apply_event(start_event)

        start_event2 = StartEvent('id', {'startTime': datetime.datetime.utcnow()})
        with self.assertRaises(RejectionEventException):
            self._aggregateRoot.apply_event(start_event2)

        self.assertEqual(len(self._aggregateRoot.get_rejection_engine().get_rejected_events()), 1)
