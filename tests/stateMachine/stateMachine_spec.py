"""
Test Aggregate Root
"""

import unittest
from fsm import EventSourcingMachine

class StateMachineTestCase(unittest.TestCase):
    """
    Test unitario Aggregate Root
    """
    def setUp(self):
        self._machine = EventSourcingMachine()

        self._machine.add_state("Inicio", initial_state=True)
        self._machine.add_state("Esperando")
        self._machine.add_state("Fin")

        self._machine.add_transition("start", "Inicio", "Esperando")
        self._machine.add_transition("stop", "Esperando", "Fin")
        self._machine.set_state("Inicio")

    def test_transite_valid_transition(self):
        ret = self._machine.transite("start")
        self.assertTrue(ret)
        self.assertEqual(self._machine.get_state(), 'Esperando')

    def test_transite_invalid_transition(self):
        ret = self._machine.transite("lala")
        self.assertFalse(ret)
        self.assertEqual(self._machine.get_state(), 'Inicio')
