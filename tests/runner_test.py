import os
import sys
import unittest

sys.path.append(os.path.abspath('../src'))

suite = unittest.TestLoader().discover("./", pattern='*_spec.py', top_level_dir=None)
unittest.TextTestRunner().run(suite)
