"""
Test Aggregate Root
"""

import unittest
from unittest.mock import Mock
from entities import AggregateRoot, DomainEvent
from rejection.engines import RejectionEventException, StopOnErrorRejectionEngine
from snapshot import SnapshotBuilder
from fsm import EventSourcingMachine
import datetime

class SnapshotTestCase(unittest.TestCase):
    """
    Test unitario Aggregate Root
    """
    def setUp(self):
        self._root = Mock()
        self._rejection_engine = Mock()
        self._root.get_applied_events = Mock(return_value=[{'evento':'1'}])
        self._root.get_rejection_engine = Mock(return_value=self._rejection_engine)
        self._rejection_engine.get_rejected_events = Mock(return_value=[])
        self._repository = Mock()
        self._snapshot = SnapshotBuilder(self._repository)

    def test_snapshot_build(self):
        self._snapshot.build(self._root)
        self._root.get_applied_events.assert_called_once()
